//
//  ViewController.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import UIKit

class BooksViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var bookTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchBar.setTextColor(color: UIColor.white)
        self.dataFetchStatus = .Completed
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bookTableView.reloadData()
    }
    
    // MARK: - Private
    
    private var dataFetchStatus : DataFetchStatus!
}

// MARK: - UISearchBarDelegate

extension BooksViewController : UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        AppManager.sharedInstance.setSearchQuery(query: searchText)
        self.bookTableView.reloadData()
    }
}

// MARK: - UITableViewDataSource

extension BooksViewController : UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        var rows = AppManager.sharedInstance.getNumberOfBooksDownloaded()
        if self.dataFetchStatus == .Started {
            rows += 1
        }
        return rows
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell!
        if indexPath.row < AppManager.sharedInstance.getNumberOfBooksDownloaded(){
            let identifier = "BookCell"
            cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! BooksTableViewCell
            (cell as! BooksTableViewCell).setupCell(id: AppManager.sharedInstance.getBookIdForIndex(index: indexPath.row))
        }
        else{
            let identifier = "LoaderCell"
            cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! LoaderTableViewCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 20, 0)
        cell.layer.transform = transform
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1.0
            cell.layer.transform = CATransform3DIdentity
        }
    }
}

// MARK: - UITableViewDelegate

extension BooksViewController : UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = 50.0
        if indexPath.row < AppManager.sharedInstance.getNumberOfBooksDownloaded() {
            height = 200.0
        }
        return CGFloat(height)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < AppManager.sharedInstance.getNumberOfBooksDownloaded() {
            AppManager.sharedInstance.selectedId = AppManager.sharedInstance.getBookIdForIndex(index: indexPath.row)
        }
    }
    
    private func lastIndexPath() -> IndexPath? {
        var indexPath : IndexPath? = nil
        let visibleRows : Array = self.bookTableView.visibleCells;
        if let lastVisibleCell : UITableViewCell = visibleRows.last {
            indexPath = self.bookTableView.indexPath(for: lastVisibleCell)
        }
        return indexPath
    }
}

