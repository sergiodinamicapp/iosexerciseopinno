//
//  BooksTableViewCell.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import UIKit

class BooksTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets

    @IBOutlet weak var bookTitleLabel: UILabel! {
        didSet {
            bookTitleLabel.textColor = UIColor.white
        }
    }
    @IBOutlet weak var bookAuthorLabel: UILabel! {
        didSet {
            bookAuthorLabel.textColor = UIColor.white
        }
    }
    @IBOutlet weak var bookPublisherLabel: UILabel! {
        didSet {
            bookPublisherLabel.textColor = UIColor.white
        }
    }
    @IBOutlet weak var bookImageView: UIImageView!

    public func setupCell(id: String) {
        self.bookTitleLabel.text = AppManager.sharedInstance.getBookTitleForId(id: id)
        self.bookAuthorLabel.text = AppManager.sharedInstance.getBookAuthorsNameForId(id: id)
        self.bookPublisherLabel.text = AppManager.sharedInstance.getBookPublishersNameForId(id: id)
        self.bookImageView.downloadImageFrom(link: AppManager.sharedInstance.getBookThumbnailForId(id: id), contentMode: UIViewContentMode.scaleAspectFit)
    }
}

class LoaderTableViewCell: UITableViewCell {}
