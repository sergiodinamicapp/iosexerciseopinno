//
//  DetailsViewController.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    // MARK: - Properties
    
    
    // MARK: - IBOutlets

    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = UIColor.white
        }
    }
    @IBOutlet weak var authorsLabel: UILabel! {
        didSet {
            authorsLabel.textColor = UIColor.white
        }
    }
    @IBOutlet weak var publisherLabel: UILabel! {
        didSet {
            publisherLabel.textColor = UIColor.white
        }
    }
    @IBOutlet weak var descriptionTextView: UITextView! {
        didSet {
            descriptionTextView.textColor = UIColor.white
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detalles"
        
        
        let id = AppManager.sharedInstance.selectedId
        self.titleLabel.text = AppManager.sharedInstance.getBookTitleForId(id: id)
        self.authorsLabel.text = AppManager.sharedInstance.getBookAuthorsNameForId(id: id)
        self.publisherLabel.text = AppManager.sharedInstance.getBookPublishersNameForId(id: id)
        self.descriptionTextView.text = AppManager.sharedInstance.getBookDescriptionForId(id: id)
        self.bookImageView.downloadImageFrom(link: AppManager.sharedInstance.getBookThumbnailForId(id: id), contentMode: UIViewContentMode.scaleAspectFit)
        
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        self.descriptionTextView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    
   
}
