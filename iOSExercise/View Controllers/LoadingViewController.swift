//
//  LaodingViewController.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        AppManager.sharedInstance.getAllBooksFromRemote { (status :Dictionary<String, Any>) in
            DispatchQueue.main.async {
                let downloadStatus = status["status"] as! String
                if downloadStatus == CompletionStatus.Success.rawValue {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "NavController") as! UINavigationController
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                }
            }
        }
    }
}
