//
//  PanelizationSummary.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation


public class PanelizationSummary {
	public var containsEpubBubbles : String?
	public var containsImageBubbles : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [PanelizationSummary]
    {
        var models:[PanelizationSummary] = []
        for item in array
        {
            models.append(PanelizationSummary(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
	required public init?(dictionary: NSDictionary) {

		containsEpubBubbles = dictionary["containsEpubBubbles"] as? String
		containsImageBubbles = dictionary["containsImageBubbles"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.containsEpubBubbles, forKey: "containsEpubBubbles")
		dictionary.setValue(self.containsImageBubbles, forKey: "containsImageBubbles")

		return dictionary
	}
}
