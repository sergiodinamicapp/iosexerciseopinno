//
//  SaleInfo.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation

public class SaleInfo {
	public var country : String?
	public var saleability : String?
	public var isEbook : String?
	public var buyLink : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [SaleInfo]
    {
        var models:[SaleInfo] = []
        for item in array
        {
            models.append(SaleInfo(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		country = dictionary["country"] as? String
		saleability = dictionary["saleability"] as? String
		isEbook = dictionary["isEbook"] as? String
		buyLink = dictionary["buyLink"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.country, forKey: "country")
		dictionary.setValue(self.saleability, forKey: "saleability")
		dictionary.setValue(self.isEbook, forKey: "isEbook")
		dictionary.setValue(self.buyLink, forKey: "buyLink")

		return dictionary
	}
}
