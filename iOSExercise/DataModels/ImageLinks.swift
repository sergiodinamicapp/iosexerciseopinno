//
//  ImageLinks.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 16/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation


public class ImageLinks {
	public var smallThumbnail : String?
	public var thumbnail : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [ImageLinks]
    {
        var models:[ImageLinks] = []
        for item in array
        {
            models.append(ImageLinks(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
	required public init?(dictionary: NSDictionary) {

		smallThumbnail = dictionary["smallThumbnail"] as? String
		thumbnail = dictionary["thumbnail"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.smallThumbnail, forKey: "smallThumbnail")
		dictionary.setValue(self.thumbnail, forKey: "thumbnail")

		return dictionary
	}
}
