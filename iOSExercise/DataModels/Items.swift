//
//  Items.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation

public class Items {
	public var kind : String?
	public var id : String?
	public var etag : String?
	public var selfLink : String?
	public var volumeInfo : VolumeInfo?
	public var saleInfo : SaleInfo?
	public var accessInfo : AccessInfo?
	public var searchInfo : SearchInfo?

    public class func modelsFromDictionaryArray(array:NSArray) -> [Items]
    {
        var models:[Items] = []
        for item in array
        {
            models.append(Items(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		kind = dictionary["kind"] as? String
		id = dictionary["id"] as? String
		etag = dictionary["etag"] as? String
		selfLink = dictionary["selfLink"] as? String
		if (dictionary["volumeInfo"] != nil) { volumeInfo = VolumeInfo(dictionary: dictionary["volumeInfo"] as! NSDictionary) }
		if (dictionary["saleInfo"] != nil) { saleInfo = SaleInfo(dictionary: dictionary["saleInfo"] as! NSDictionary) }
		if (dictionary["accessInfo"] != nil) { accessInfo = AccessInfo(dictionary: dictionary["accessInfo"] as! NSDictionary) }
		if (dictionary["searchInfo"] != nil) { searchInfo = SearchInfo(dictionary: dictionary["searchInfo"] as! NSDictionary) }
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.kind, forKey: "kind")
		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.etag, forKey: "etag")
		dictionary.setValue(self.selfLink, forKey: "selfLink")
		dictionary.setValue(self.volumeInfo?.dictionaryRepresentation(), forKey: "volumeInfo")
		dictionary.setValue(self.saleInfo?.dictionaryRepresentation(), forKey: "saleInfo")
		dictionary.setValue(self.accessInfo?.dictionaryRepresentation(), forKey: "accessInfo")
		dictionary.setValue(self.searchInfo?.dictionaryRepresentation(), forKey: "searchInfo")

		return dictionary
	}
}
