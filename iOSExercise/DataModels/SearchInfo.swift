//
//  SearchInfo.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation


public class SearchInfo {
	public var textSnippet : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [SearchInfo]
    {
        var models:[SearchInfo] = []
        for item in array
        {
            models.append(SearchInfo(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		textSnippet = dictionary["textSnippet"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.textSnippet, forKey: "textSnippet")

		return dictionary
	}
}
