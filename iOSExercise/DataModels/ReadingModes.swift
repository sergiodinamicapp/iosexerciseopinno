//
//  ReadingModes.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 17/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation


public class ReadingModes {
	public var text : String?
	public var image : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [ReadingModes]
    {
        var models:[ReadingModes] = []
        for item in array
        {
            models.append(ReadingModes(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		text = dictionary["text"] as? String
		image = dictionary["image"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.text, forKey: "text")
		dictionary.setValue(self.image, forKey: "image")

		return dictionary
	}
}
