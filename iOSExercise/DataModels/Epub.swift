//
//  Epub.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 16/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation


public class Epub {
	public var isAvailable : String?
	public var downloadLink : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [Epub]
    {
        var models:[Epub] = []
        for item in array
        {
            models.append(Epub(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		isAvailable = dictionary["isAvailable"] as? String
		downloadLink = dictionary["downloadLink"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.isAvailable, forKey: "isAvailable")
		dictionary.setValue(self.downloadLink, forKey: "downloadLink")

		return dictionary
	}
}
