//
//  IndustryIdentifiers.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 16/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation


public class IndustryIdentifiers {
	public var type : String?
	public var identifier : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [IndustryIdentifiers]
    {
        var models:[IndustryIdentifiers] = []
        for item in array
        {
            models.append(IndustryIdentifiers(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
	required public init?(dictionary: NSDictionary) {

		type = dictionary["type"] as? String
		identifier = dictionary["identifier"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.identifier, forKey: "identifier")

		return dictionary
	}
}
