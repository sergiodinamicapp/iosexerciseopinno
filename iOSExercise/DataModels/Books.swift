//
//  Books.swift
//  iOSExercise Opinno
//
//  Created by Sergio on 16/12/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

import Foundation

public class Books {
	public var kind : String?
	public var totalItems : Int?
	public var items : Array<Items>?
    public class func modelsFromDictionaryArray(array:NSArray) -> [Books]
    {
        var models:[Books] = []
        for item in array
        {
            models.append(Books(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
	required public init?(dictionary: NSDictionary) {

		kind = dictionary["kind"] as? String
		totalItems = dictionary["totalItems"] as? Int
        if (dictionary["items"] != nil) { items = Items.modelsFromDictionaryArray(array: dictionary["items"] as! NSArray) }
	}
    
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.kind, forKey: "kind")
		dictionary.setValue(self.totalItems, forKey: "totalItems")

		return dictionary
	}
}
